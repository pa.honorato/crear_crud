from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def home(request):
    cliente = Cliente.objects.all()
    return render(request, 'gestionCurso.html', {"cursos": cliente})

def registrarCliente(request):
    rut = request.POST['txtRut']
    nombre = request.POST['txtNombre']
    apellido = request.POST['txtApellido']
    edad = request.POST['numEdad']
    correo = request.POST['txtCorreo']
    telefono = request.POST['num']
    
    cliente= Cliente.objects.create(
        rut= rut, nombre= nombre, apellido= apellido,edad=edad,correo=correo, telefono=telefono)
    return redirect('/')
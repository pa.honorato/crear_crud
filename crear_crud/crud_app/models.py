from django.db import models

# Create your models here.

class Persona(models.Model):
    rut = models.CharField(primary_key=True, max_length=50)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    edad = models.SmallIntegerField()
    correo = models.CharField(max_length=100)

    def __str__(self):
        texto = "{0}({1})"
        return texto.format(self.nombre, self.correo)